Revival of `csemlib`.

I suggest using a python virtual environment for the library development. 

How to use it:

1. Initiate a virtual environment:
```
python3 -m venv csemenv
```

2. Activate the environment:
```
source csemlib/bin/activate
```

3. Install the packages from the [`requirements`](https://gitlab.com/swp_ethz/public/csemlib_v2/-/blob/main/requirements_csemenv.txt) file:
```
python3 -m pip install -r requirements_csemenv.txt
```

4. To deactivate the virtual environment run
```
deactivate
```

If any new dependency pops up, please update the `requirements_csemenv.txt` file by running:
```
pip freeze > requirements_csemenv.txt
```



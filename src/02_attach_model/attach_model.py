import numpy as np


def attach_model(point_crd: np.ndarray, point_layer: np.ndarray, model: str) -> dict:
    """Forms an xarray.Dataset containing the model for a point cloud.

    Keyword arguments:
        point_crd   -- coordinates of the points in the point cloud
        point_layer -- array of indeces of the layer that the point belongs to
        model       -- name of the structural model

    Returns:
        dictionary with the point coordinates, the physical parameters from the model,
        name of the model
    """
    SUPPORTED_MODELS = ["PREM"]
    # Input parameter tests:
    assert (
        model in SUPPORTED_MODELS
    ), f"Input model be one of the {SUPPORTED_MODELS}. {model} is not supported."

    # make sure that the input arrays are np.ndarray
    assert len(point_crd) == len(
        point_layer
    ), "point_crd and point_layer have different dimensions."

    ##########
    # TO BE IMPLEMENTED:
    # evaluation of the physical parameters at the points
    ##########

    output_dict = dict(
        coordinates=point_crd,
        layers=point_layer,
        # add RHO, VP, VS etc.
    )
    return output_dict

#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Mapping of lagrangian basis derivatives into a tensor basis in 2D and 3D +
nodal jacobian

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2019
:license:
    None
"""
import numpy as np
from .basis_polynomials import lagrange_basis_derivative_matrix as D_lagrange
from .quadrature_points_weights import (
    gauss_lobatto_legendre_quadruature_points_weights as points_weights,
)


def get_Dn(p, ndim=2):
    """
    compute elemental derivative matrices for 2 or 3 dimensional tensore basis
    with lagrange polynomials on points p
    """

    D = D_lagrange(p)

    ngll = len(p)
    npoints = ngll**ndim

    Dn = np.zeros((ndim, npoints, npoints))

    if ndim == 2:
        for idim in range(ndim):
            stride = ngll**idim
            for irow in range(ngll):
                offset = irow * ngll ** [1, 0][idim]
                end = offset + stride * ngll
                Dn[
                    idim, offset : offset + stride * ngll : stride, offset:end:stride
                ] = D

    elif ndim == 3:
        for idim in range(ndim):
            stride = ngll**idim
            for icol in range(ngll):
                for irow in range(ngll):
                    offset = (
                        icol * ngll ** [2, 2, 1][idim] + irow * ngll ** [1, 0, 0][idim]
                    )
                    end = offset + stride * ngll
                    Dn[
                        idim,
                        offset : offset + stride * ngll : stride,
                        offset:end:stride,
                    ] = D
    else:
        raise NotImplementedError

    return [Dn[i] for i in range(ndim)]


def get_jacobian(m):
    """
    Compute the jacobian matrix for arbitrary order basis polynomials on a
    salvus.mesh.UnstructuredMesh object
    """

    J = np.zeros((m.nelem, m.nodes_per_element, m.ndim, m.ndim))

    p, w = points_weights(m.shape_order + 1)
    Dn = get_Dn(p, m.ndim)

    pp = m.points[m.connectivity[:, :]]
    for n in range(m.nodes_per_element):

        for idim in range(m.ndim):
            J[:, n, idim, :] = Dn[idim][n].dot(pp)

    return J


def get_jacobian_determinant(m):
    """
    Compute the determinant of the jacobian matrix for arbitrary order basis
    polynomials on a salvus.mesh.UnstructuredMesh object

    separate function to be more memory efficient, but the result should be
    equal to:
        np.linalg.det(get_jacobian(m))
    """
    J = np.zeros((m.nelem, m.ndim, m.ndim))
    detJ = np.zeros((m.nelem, m.nodes_per_element))

    p, w = points_weights(m.shape_order + 1)
    Dn = get_Dn(p, m.ndim)

    pp = m.points[m.connectivity[:, :]]
    for n in range(m.nodes_per_element):

        for idim in range(m.ndim):
            J[:, idim, :] = Dn[idim][n].dot(pp)

        detJ[:, n] = np.linalg.det(J)

    return detJ


def get_integration_weights(w, ndim=2):
    ngll = len(w)
    npoints = ngll**ndim

    Wn = np.zeros(npoints)

    if ndim == 2:
        idx = 0
        for i in range(ngll):
            for j in range(ngll):
                Wn[idx] = w[i] * w[j]
                idx += 1

    elif ndim == 3:
        idx = 0
        for i in range(ngll):
            for j in range(ngll):
                for k in range(ngll):
                    Wn[idx] = w[i] * w[j] * w[k]
                    idx += 1

    return Wn


def get_mass_matrix(m):
    """
    get unassembled diagonal mass matrix as elemental vectors
    """
    p, w = points_weights(m.shape_order + 1)
    w_ndim = get_integration_weights(w, m.ndim)
    detJ = get_jacobian_determinant(m)

    return w_ndim[np.newaxis, :] * detJ

#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Mapping of lagrangian basis derivatives into a tensor basis in 2D and 3D +
nodal jacobian

:copyright:
    Martin van Driel (Martin@vanDriel.de), 2019
:license:
    None
"""
from scipy import sparse
from salvus.mesh.global_numbering import unique_points

from elemental_matrices import get_mass_matrix as gmm


def get_gather_scatter(points):
    """
    get gather and scatter operators for scalar problems as sparse matrix
    """
    # get global ids from lexicographic sorting
    p, global_ids = unique_points(points)

    # set up gather = boolean matrix mapping from local to global DOFs
    npoints_local = points.shape[0]
    npoints_global = p.shape[0]

    gather = sparse.lil_matrix((npoints_global, npoints_local))
    gather[global_ids, range(npoints_local)] = 1

    gather = gather.tocsr()
    scatter = gather.T
    return gather, scatter, p, global_ids


def get_mass_matrix_points(m):
    """
    get assembled diagonal mass matrix as sparse matrix
    """
    M = sparse.diags(gmm(m).ravel()).tocsr()
    p = m.points[m.connectivity].reshape((m.nelem * m.nodes_per_element, m.ndim))
    gather, scatter, p, global_ids = get_gather_scatter(p)

    return gather.dot(M).dot(scatter), p

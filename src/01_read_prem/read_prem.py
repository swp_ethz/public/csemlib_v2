# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
from numpy import genfromtxt


def read_prem(inputfile):
    """Return a dictionary containing prem fields. Requires the specific file format.
    The input file can be downloaded from https://ds.iris.edu/spud/earthmodel/9991844

    Keyword arguments:
    inputfile -- path to the prem model file

    """
    header = [
        "radius",
        "depth",
        "density",
        "Vpv",
        "Vph",
        "Vsv",
        "Vsh",
        "eta",
        "Q-mu",
        "Q-kappa",
    ]
    dataread = genfromtxt(inputfile, delimiter=",")
    data = {label: d for label, d in zip(header, dataread.T)}
    return data


def get_discontinuities(radii) -> list:
    """Returns a list of repeating radii
    which is a conventional way of marking discontinuitites"""
    disc_r = (
        [
            radii[0],
        ]
        + [radii[i] for i in range(1, len(radii)) if radii[i] == radii[i - 1]]
        + [
            radii[-1],
        ]
    )
    return disc_r


def plot_profiles(data: dict, fields_to_plot=["Vph", "Vpv"], discontinuities=[]):
    """
    Plot 1D profiles for the parameter fields against radius.

    Keynote arguments:
    data            -- dictionary containing the parameter fields
    fields_to_plot  -- names of the dictionary fields to be plotted
    discontinuities -- list of the discontinuity radii plotted as horizontal lines
    """
    assert all(
        key in data.keys() for key in fields_to_plot
    ), f"fields_to_plot should include only the following fields: {data.keys()}"
    import matplotlib.pyplot as plt

    fig, ax = plt.subplots(figsize=(5, 10))

    for field in fields_to_plot:
        ax.plot(data[field], data["radius"], label=field)
    ax.legend()
    ax.set_ylabel("Radius, km")

    if len(discontinuities) > 0:
        for disc_r in discontinuities:
            ax.axhline(disc_r, **{"linestyle": "--", "linewidth": 0.5})
    return fig


# -

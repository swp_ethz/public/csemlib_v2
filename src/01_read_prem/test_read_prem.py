import pytest
from numpy import testing
from read_prem import get_discontinuities


@pytest.mark.parametrize(
    "radii, expected_disconts",
    [
        (
            [0.0, 100.0, 100.0, 200.0, 300.0, 400.0, 500.0, 500.0, 600.0],
            [0.0, 100.0, 500.0, 600.0],
        ),
    ],
)
def test_get_discontinuities(radii, expected_disconts):
    testing.assert_equal(actual=get_discontinuities(radii), desired=expected_disconts)
